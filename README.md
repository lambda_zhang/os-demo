## 1 embox
Ubuntu上安装qemu
```
$ sudo apt-get install qemu-system-arm
```

```
编译
$ cd embox
$ make confload-arm/qemu
$ make

运行
$ qemu-system-arm -M integratorcp -kernel ./build/base/bin/embox -m 256 -nographic
```


### 2 FreeRTOS
Ubuntu上安装qemu 和工具链
```
$ sudo apt-get install qemu-system-arm binutils-arm-none-eabi gcc-arm-none-eabi
```

```
编译
$ cd FreeRTOS/FreeRTOS/Demo/CORTEX_LM3S811_GCC/

运行
$ qemu-system-arm -machine lm3s811evb -kernel gcc/RTOSDemo.axf -m 256 -nographic
```


### 3 NuttX
源码位置
```
https://github.com/apache/incubator-nuttx
https://github.com/apache/incubator-nuttx-apps
https://bitbucket.org/nuttx/tools/src/master/
```

```
$ sudo apt install flex bison gperf libtool
```

```
$ cd nuttx/
$ cd tools/kconfig-frontends/
$ ./configure
$ autoreconf -ivf
$ make
$ sudo make install
$ sudo ldconfig

$ cd ../..
$ cd nuttx
$ make distclean
$ cd tools/
$ ./configure.sh lm3s6965-ek/nsh
$ cd ..
$ make menuconfig
打开如下配置
System Type  ---> 
    Stellaris Ethernet Configuration  --->
        [*] Workaround for tiva with qemu
把工具链改为linux本地的
System Type  ---> 
    Toolchain Selection (Generic GNU EABI toolchain under Linux (or other POSIX environment))  --->
        (X) Generic GNU EABI toolchain under Linux (or other POSIX

$ make

$ ls nuttx.bin -lh
-rwxrwxr-x 1 lambda lambda 43K 9月  27 14:02 nuttx.bin
```

```
$ qemu-system-arm -M lm3s6965evb -kernel nuttx -m 256 -nographic

NuttShell (NSH)
nsh> 
nsh> ?
help usage:  help [-v] [<cmd>]

  .         cat       echo      hexdump   mkrd      put       source    umount    
  [         cd        exec      kill      mh        pwd       test      unset     
  ?         cp        exit      ls        mount     rm        telnetd   usleep    
  arp       cmp       false     mb        mv        rmdir     time      wget      
  basename  dirname   get       mkdir     mw        set       true      xd        
  break     dd        help      mkfatfs   nslookup  sleep     uname   
```
